package com.ppb.kelas1.newsapik1

import com.ppb.kelas1.newsapik1.data.NewsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    //baseurl = https://newsapi.org/v2/
    //everything?q=jokowi&from=2024-03-29&language=id&apiKey=6c49aec93ddb4927ae2e1cf47bd218eb
    @GET("everything")
    fun getNewsData(
        @Query("q") search: String,
        @Query("from") from: String,
        @Query("language") language: String,
        @Query("apiKey") apiKey: String
    ): Call<NewsResponse>
}