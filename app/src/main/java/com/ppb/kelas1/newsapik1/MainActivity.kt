package com.ppb.kelas1.newsapik1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ppb.kelas1.newsapik1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: ArticleAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initRecyclerView()
        viewModel.newsResponse.observe(this) { response ->
            adapter.articles = response.articles
        }
        viewModel.getNewsFromAPI("pemilu", "2024-04-20")
    }

    fun initRecyclerView() {
        binding.rvArticle.layoutManager = LinearLayoutManager(this)
        adapter = ArticleAdapter()
        binding.rvArticle.adapter = adapter
    }
}